package ro;


import ro.DictionaryOperations.*;
import ro.FileReader.GlobalInputFileReader;
import ro.FileReader.InputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] arg) throws IOException {
        List<String> allLines = new ArrayList<>();

        InputFileReader inputFileReader = new GlobalInputFileReader();
        allLines = inputFileReader.readFile("C:\\CODE\\dictionary-project\\src\\main\\resources\\dex.txt");
        //aduce toate informatiile din dex file

        Set<String> wordsSet = Utils.removeDuplicates(allLines);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            //creez meniul
            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Find all Palindroms\n\t" +
                    "3. Load random word\n\t" +
                    "4. Find anagrams\n\t" +
                    "0. Exiting");
            String userMenuSelection = in.readLine();

            //Exit if user selected 0
            if (userMenuSelection.equals("0")) {
                System.out.println("Exiting....");
                break;
            }

            DictionaryOperation operation = null;
            switch (userMenuSelection) {
                //va face call la fidWordsThatContains din Utils
                case "1":
                    System.out.println("Input your string");
                    operation = new SearchDictionaryOperation(wordsSet, in);

                    break;
                case "2":
                    operation = new FindPalindroms(wordsSet);
                    break;
                case "3":
                    System.out.println("A random word from the dictionary is listed for you");
                    operation = new RandomWordDictionaryOperation(wordsSet);
                    break;
                case "4":
                    System.out.println("Input a word and we will check if this is an anagram");
                    operation = new SearchAnagrams(wordsSet,in);
                   // operation.run();
                    break;

                default:
                    System.out.println("Please select a valid option");
            }
            if (operation != null) {
                operation.run();
            }

        }
    }
}





