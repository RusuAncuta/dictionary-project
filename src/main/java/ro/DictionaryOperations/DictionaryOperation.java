package ro.DictionaryOperations;

import java.io.IOException;

public interface DictionaryOperation {
    void run() throws IOException;
}
