package ro;

import org.junit.Test;
import ro.DictionaryOperations.DictionaryOperation;
import ro.DictionaryOperations.SearchAnagrams;
import ro.FileReader.GlobalInputFileReader;
import ro.FileReader.InputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestClass {

    @Test
    public void testPalindromFinderIdentifiesPalindromes() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
    }

    @Test
    public void testPalindromFinder_FindsMultiple() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("ata");
        inputSet.add("cojoc");
        inputSet.add("penar");//verific daca inputul este un palindrom

        Set<String> palindromes = Utils.findPalindromes(inputSet);
        assertEquals(2, palindromes.size());
    }


    @Test
    public void testPalindromesFinderNull() {
        Set<String> palindromes = Utils.findPalindromes(null);
        assertEquals(0, palindromes.size());
    }


    @Test
    public void testSortedLetters() {
        List<String> wordsList = new ArrayList<>();
        List<String> sortedList = new ArrayList<>();
        wordsList.add("acalmie");
        wordsList.add("bumbacie");
        wordsList.add("balamuc");
        wordsList.add("amicale");
        wordsList.add("camelia");
        String word = "macalei";
        for (int i = 0; i < wordsList.size(); i++) {
            //System.out.println(Utils.sortingLetters(word));
            System.out.println(Utils.sortingLetters(wordsList.get(i)));

            System.out.println("-------------------------------------");
            if (Utils.sortingLetters(wordsList.get(i)).equals(Utils.sortingLetters(word))) {
                sortedList.add(wordsList.get(i));
            }
        }
        System.out.println("-------------------------------------");

        for (String b : sortedList) {
            System.out.println(b);
        }
        assertEquals(Utils.sortingLetters(word), Utils.sortingLetters(wordsList.get(0)));
        System.out.println("-------------------------------------");

        //System.out.println(Utils.anagramList(wordsList,word));

    }


    @Test
    public void testAnagrams() throws IOException {
        List<String> allWords = new ArrayList<>();
        InputFileReader inputFileReader = new GlobalInputFileReader();
        allWords = inputFileReader.readFile("C:\\CODE\\dictionary-project\\src\\main\\resources\\dex.txt");
        assertTrue(allWords.contains("ca"));
        String word = "calma";
        Set<String> setWithWordd=new TreeSet<>();
        for(String e:allWords){
            setWithWordd.add(e);
        }
        System.out.println(Utils.mapWithAnagrams(setWithWordd, word));
    }

    @Test
    public void testRandomFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("1");
        inputSet.add("2");
        inputSet.add("3");
        inputSet.add("4");
        String randomWord = Utils.getRandomWordFromSet(inputSet, new Random(1L));
        assertEquals("3", randomWord);


    }


    @Test
    public void testAnagrams2() {
        Set<String> wordList = new HashSet<>();
        wordList.add("arc");
        wordList.add("arc");
        wordList.add("rac");
        wordList.add("cra");
        wordList.add("bondoc");
        wordList.add("nascocire");
        String word = "arc";
        Map<String, Set<String>> map = Utils.mapWithAnagrams(wordList, word);
        System.out.println(map);

    }

    @Test
    public void testAngramsOP() throws IOException {
        Set<String> wordList = new HashSet<>();
        wordList.add("arc");
        wordList.add("arc");
        wordList.add("rac");
        wordList.add("cra");
        wordList.add("bondoc");
        wordList.add("nascocire");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String word = in.readLine();
        DictionaryOperation opretaions = new SearchAnagrams(wordList, in);
        opretaions.run();

    }
}
